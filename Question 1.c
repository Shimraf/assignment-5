#include <stdio.h>

int noOfAttendence(int price);
int revenue(int price);
int cost(int price);
int profit(int price);


int noOfAttendence(int price){
   return 120-(price-15)/5*20;
}
int revenue(int price){
  return noOfAttendence(price)*price;
}
int cost(int price){
return noOfAttendence(price)*3+500;
}
int profit(int price){
 return revenue(price)-cost(price);
}

int main(){
int price;
printf("Expected profit for ticket prices :\n\n");
for(price=5;price<=50;price+=5){

    printf("When the price of a ticket is %d,the profit gain =%d\n",price,profit(price));

}
return 0;
}
